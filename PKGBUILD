# Maintainer: Bruno Pagani <archange@archlinux.org>

_pkg=param
pkgname=python-${_pkg}
pkgver=2.1.0
pkgrel=1
pkgdesc="Make your Python code clearer and more reliable by declaring Parameters"
arch=(any)
url="https://param.holoviz.org"
license=(BSD-3-Clause)
depends=(python)
makedepends=(python-build python-installer python-hatch-vcs python-wheel)
checkdepends=(python-pytest-asyncio python-jsonschema python-numpy python-pandas ipython)
# No tests in Pypi tarballs
#source=(https://files.pythonhosted.org/packages/source/${_pkg::1}/${_pkg}/${_pkg}-${pkgver}.tar.gz)
source=(https://github.com/holoviz/param/archive/v${pkgver}/${_pkg}-${pkgver}.tar.gz)
sha256sums=('6efd8374749ca3e6bf6090405d3f9fb73221995abe4a26da2c2babfb65c7108e')

build() {
  cd ${_pkg}-${pkgver}
  export SETUPTOOLS_SCM_PRETEND_VERSION=${pkgver}
  python -m build --wheel --skip-dependency-check --no-isolation
}

check() {
  cd ${_pkg}-${pkgver}
  PARAM_TEST_JSONSCHEMA=1 PARAM_TEST_NUMPY=1 PARAM_TEST_PANDAS=1 PARAM_TEST_IPYTHON=1 \
    pytest tests -vv --color=yes \
    --ignore=tests/testfiledeserialization.py
}

package() {
  cd ${_pkg}-${pkgver}
  python -m installer --destdir="${pkgdir}" dist/*.whl
  install -Dm644 LICENSE.txt -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
